(function ($, Drupal, drupalSettings) {
    Drupal.behaviors.checkUsername = {
        attach: function (context, settings) {
            // Checking username field uniqueness.
            $("#username-check-informer:not(.username-check-processed)", context).each(function () {
                let input = $(this).parents(".form-item").find('input[name="name"]');
                Drupal.checkUsername.username = "";
                let delayed = Drupal.checkUsername.delay;
                input.keyup(function () {
                    if (input.val() !== Drupal.checkUsername.username) {
                        clearTimeout(Drupal.checkUsername.timer);
                        Drupal.checkUsername.timer = setTimeout(function () {
                            Drupal.checkUsername.check(input, settings);
                        }, delayed);

                        if (
                          !$("#username-check-informer").hasClass(
                            "username-check-informer-progress"
                          )
                        ) {
                            $("#username-check-informer")
                              .removeClass("username-check-informer-accepted")
                              .removeClass("username-check-informer-rejected");
                        }
                        $("#username-check-message").hide();
                    }
                  }).blur(function () {
                      if (input.val() !== Drupal.checkUsername.username) {
                         Drupal.checkUsername.check(input, settings);
                      }
                });
            }).addClass("username-check-processed");
        },
    };

    Drupal.checkUsername = {};
    Drupal.checkUsername.check = function (input, settings) {
    clearTimeout(Drupal.checkUsername.timer);
    Drupal.checkUsername.username = input.val();
    let uid = settings.checkUsername.uid;
    console.log(uid);
    $.ajax({
      url: settings.checkUsername.ajaxUrl,
      method: "POST",
      data: { username: Drupal.checkUsername.username, uid: uid},
      dataType: "json",
      beforeSend: function () {
        $("#username-check-informer")
          .removeClass("username-check-informer-accepted")
          .removeClass("username-check-informer-rejected")
          .addClass("username-check-informer-progress");
      },
      success: function (ret) {
        if (ret["allowed"]) {
          $("#username-check-informer")
            .removeClass("username-check-informer-progress")
            .addClass("username-check-informer-accepted");
          input.removeClass("error");
          if (input.val() !== input.data("value")) {
            $("#edit-submit").prop("disabled", FALSE);
          }
        } else {
          $("#username-check-informer")
            .removeClass("username-check-informer-progress")
            .addClass("username-check-informer-rejected");

          $("#username-check-message")
            .addClass("username-check-message-rejected")
            .html(ret["msg"])
            .show();
          $("#edit-submit").prop("disabled", TRUE);
        }
      },
    });
  };
})(jQuery, Drupal, drupalSettings);
