# Check Username

This very simple module allows visitors to check username originality quickly using AJAX.
checking the username on the registration form and when editing the profile.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/check_username).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/check_username).


## Requirements

This module requires no modules outside of Drupal core.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

Configure delay time for checking process.

access config form: <b> admin > configuration > system > check username<b>.

<b>/admin/config/system/check-username<b>.


## Maintainers

- [Mahmoud Sayed (MahmoudSayed96)](https://www.drupal.org/u/mahmoudsayed96)
