<?php

namespace Drupal\check_username\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Returns responses for check_username routes.
 */
class CheckUsernameController extends ControllerBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   */
  protected $entityTypeManager;

  /**
   * Logger channel.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger
   */
  protected $logger;

  /**
   * The CheckUsernameController constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager, LoggerChannelFactoryInterface $logger) {
    $this->entityTypeManager = $entityTypeManager;
    $this->logger = $logger->get('check_username');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('logger.factory')
    );
  }

  /**
   * Checks if the username is unique for user.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   */
  public function checkUsername(Request $request): JsonResponse {
    $uid = $request->get('uid');
    $allowed = TRUE;
    $msg = '';
    try {
      $username = $request->get('username');
      $query = $this->entityTypeManager->getStorage('user')
        ->getQuery()
        ->accessCheck(FALSE);
      $query->condition('name', $username);
      if ($uid) {
        $query = $query->condition('uid', $uid, '<>');
      }
      $ids = $query->execute();
      if ($ids) {
        $allowed = FALSE;
        $msg = $this->t('The name %username is already taken.', ['%username' => $username]);
      }
    }
    catch (\Throwable $e) {
      $allowed = FALSE;
      $msg = $this->t('The user with id %uid does not exist.', ['%uid' => $uid]);
      $this->logger->error(__FUNCTION__ . ' ' . $e->getMessage());
    }
    return new JsonResponse(['allowed' => $allowed, 'msg' => $msg]);
  }

}
