<?php

namespace Drupal\check_username\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure check_username settings for this site.
 */
class CheckUsernameConfigForm extends ConfigFormBase {

  /**
   * Config key.
   *
   * @var string
   */
  public const CHECK_USERNAME_CONFIG_KEY = 'check_username.configs';

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'check_username_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [self::CHECK_USERNAME_CONFIG_KEY];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['delay'] = [
      '#type' => 'number',
      '#title' => $this->t('Delay time'),
      '#required' => TRUE,
      '#description' => $this->t('Delay time for checking process'),
      '#default_value' => $this->config(self::CHECK_USERNAME_CONFIG_KEY)->get('delay') ?? 5000,
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $delay = $form_state->getValue('delay');
    if (!is_numeric($delay) || $delay === 0) {
      $form_state->setErrorByName('delay', $this->t('The value is not correct.'));
    }
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config(self::CHECK_USERNAME_CONFIG_KEY)
      ->set('delay', $form_state->getValue('delay'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
